#!/bin/bash

local_branches() {
  git for-each-ref --format="%(refname:short)" refs/heads
}
# Returns the name of the current branch
current_branch() {
  git symbolic-ref --short HEAD
}

saved_branch=$(current_branch)

[[ "${saved_branch}" != "master" ]] && git checkout "master"
git pull

  if [[ "${current_branch}" != "master" ]]; then
    echo
    git checkout "master"
    git merge "${branch}"
  fi


echo
[[ "${saved_branch}" != "$(current_branch)" ]] && git checkout "${saved_branch}"
