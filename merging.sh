#!/bin/bash
# Automatically merge Development branch to master branch
git config --global credential.helper 'cache --timeout 360000'

git pull origin developer

echo "Checking out master branch"
git checkout master
git pull origin master

echo "Merging develop branch"
git merge developer

echo "Pushing commits and tags"
git push origin master
git push --tags

echo "Checking out develop branch"
git checkout developer
git pull
git push



